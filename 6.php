<?php

$a = 10;
$b = 9;

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 6</title>
</head>

<body>
    <?php

    if ($a > $b) {
        echo "El mayor es {$a}";
    } else {
        echo "El mayor es {$b}";
    }
    ?>
</body>

</html>