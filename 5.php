<?php
$a = [
    "Lunes" => 100,
    "Martes" => 150,
    "Miercoles" => 300,
    "Jueves" => 10,
    "Viernes" => 50,
];

$b = "Miercoles";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5</title>
</head>

<body>
    <?php

    // el resultado da error porque en la funcion en el primer argumento hay que pasarle
    //el elemento que quieres saber si esta en el array y despues el array
    // if (array_key_exists($a, $b)) {
    //     echo $a[$b];
    // } else {
    //     echo "no se";
    // }
    // la solucion seria esta
    if (array_key_exists($b, $a)) {
        echo $a[$b];
    } else {
        echo "no se";
    }
    // muestra el valor que tiene el indice coincidente del array que en este caso es 300

    ?>
</body>

</html>