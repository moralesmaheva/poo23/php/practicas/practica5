<?php
$a = 10;
$b = 3;

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ehercicio 2</title>
</head>

<body>
    <?php
    //la salida es punto 1 y punto 3
    if (($a % 2) == 0) {
        echo "punto 1";
    } else {
        echo "punto 2";
    }
    echo "punto 3";

    //la salida es punto 2 y punto 3
    if (($b % 2) == 0) {
        echo "punto 1";
    } else {
        echo "punto 2";
    }
    echo "punto 3";
    ?>

</body>

</html>