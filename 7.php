<?php
$dia = 6;
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 7</title>
</head>

<body>
    <?php

    //con if
    if ($dia == 1) {
        echo "Es lunes";
    } elseif ($dia == 2) {
        echo "Es martes";
    } elseif ($dia == 3) {
        echo "Es miercoles";
    } elseif ($dia == 4) {
        echo "Es jueves";
    } elseif ($dia == 5) {
        echo "Es viernes";
    } elseif ($dia == 6) {
        echo "Es sabado";
    } elseif ($dia == 7) {
        echo "Es domingo";
    } else {
        echo "No es correcto";
    };
    echo "<br>";
    //con switch
    switch ($dia) {
        case 1:
            echo "Es lunes";
            break;
        case 2:
            echo "Es martes";
            break;
        case 3:
            echo "Es miercoles";
            break;
        case 4:
            echo "Es jueves";
            break;
        case 5:
            echo "Es viernes";
            break;
        case 6:
            echo "Es sabado";
            break;
        case 7:
            echo "Es domingo";
            break;
        default:
            echo "No es correcto";
            break;
    };

    ?>
</body>

</html>