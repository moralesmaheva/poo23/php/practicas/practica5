<?php

$a = [
    "Lunes" => 100,
    "Martes" => 150,
    "Miercoles" => 300,
    "Jueves" => 10,
    "Viernes" => 50,
];

$b = "Lunes";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>

<body>
    <?php

    switch ($b) {
        case "Lunes":
            echo $a["Lunes"];
            break;
        case "Martes":
            echo $a["Martes"];
            break;
        case "Miercoles":
            echo $a["Miercoles"];
            break;
        case "Jueves":
            echo $a["Jueves"];
            break;
        case "Viernes":
            echo $a["Viernes"];
            break;
        default:
            echo "no se";
            break;
    };
    // la salida es 100 
    ?>
</body>

</html>