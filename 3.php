<?php
$a = 190;
$b = ["poco", "algo", "medio", "mucho", "enorme",];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3</title>
</head>

<body>
    <?php

    if ($a < 10) {
        echo $b[0];     //$a=5
    } elseif ($a < 20) {
        echo $b[1];     //$a=13
    } elseif ($a < 30) {
        echo $b[2];     //$a=26
    } elseif ($a < 40) {
        echo $b[3];     //$a=34
    } else {
        echo $b[4];     //$a=97
    };

    ?>
</body>

</html>